//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	// XB
	{"",	"/home/xavier/.local/bin/statusbar/cpu",	10,	 1},
	{"",	"/home/xavier/.local/bin/statusbar/cpubars",	10,	 2},
	{"",	"/home/xavier/.local/bin/statusbar/memory",	10,	 3},
	{"",	"/home/xavier/.local/bin/statusbar/moonphase",	18000,	 4},
	{"",	"/home/xavier/.local/bin/statusbar/forecast",	18000,	 5},
//	{"",	"/home/xavier/.local/bin/statusbar/news",	0,	 6},
//	{"",	"/home/xavier/.local/bin/statusbar/nettraf",	1,	 7},
	{"",	"/home/xavier/.local/bin/statusbar/internet",	5,	 8},
	{"",	"/home/xavier/.local/bin/statusbar/battery",	5,	 9},
	{"",	"/home/xavier/.local/bin/statusbar/volume",	0,	10}, // kill -44 $(pidof dwmblocks).  Add 34 to signal number
	{"",	"/home/xavier/.local/bin/statusbar/clock",	60,	11},
//	{"⌨",	"/home/xavier/.local/bin/statusbar/kbselect",	 0,	12},
	{"⌨",	"/home/xavier/.local/bin/statusbar/st.launch-keyboard",	 0,	12},
	{"",	"/home/xavier/.local/bin/statusbar/help-icon",	 0,	13},
	// XB
};

//Sets delimiter between status commands. NULL character ('\0') means no delimiter.
static char *delim = " ";

// Have dwmblocks automatically recompile and run when you edit this file in
// vim or nvim with the following line in your vimrc/init.vim:
// autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }
